#include <OneWire.h> 
#include <DallasTemperature.h>
#define ONE_WIRE_BUS 4
OneWire oneWire(ONE_WIRE_BUS); 
DallasTemperature temp_sensors(&oneWire);
#define REPORTING_PERIOD 1100
float temperature;
uint32_t lastReport = 0;

float ecg=0.0;

#include <Wire.h>
#include "MAX30100_PulseOximeter.h"
PulseOximeter pox;
uint8_t spO2=2;
float hr=2.0;

void onBeatDetected()
{
    Serial.print("");
}

void setup() {
  // initialize the serial communication:
  Serial.begin(9600);

  // PULSOSSIMETRO
  if (!pox.begin()) {
        Serial.println("FAILED");
        for(;;);
    } else {
        Serial.println("SUCCESS");
    }
  pox.setOnBeatDetectedCallback(onBeatDetected);

  // temperatura
  temp_sensors.begin(); 

  // ECG
  pinMode(10, INPUT); // Setup for leads off detection LO +
  pinMode(11, INPUT); // Setup for leads off detection LO -

}

void loop() {
  pox.update();
  
  if(!((digitalRead(10) == 1)||(digitalRead(11) == 1))){
    ecg = analogRead(A0);
  }  

  if (millis() - lastReport > REPORTING_PERIOD) {
      // pulsossimetro
      hr = pox.getHeartRate();
      spO2 = pox.getSpO2();
      
      // temp 
      temp_sensors.requestTemperatures(); // Command to get temperature readings 
      temperature = temp_sensors.getTempCByIndex(0); // First sensor

      lastReport = millis();
      
  }
  Serial.print(ecg);
  Serial.print(",");
  Serial.print(temperature);
  Serial.print(",");
  Serial.print(hr);
  Serial.print(",");
  Serial.print(spO2);
  Serial.print("\n");
  delay(10);
}
