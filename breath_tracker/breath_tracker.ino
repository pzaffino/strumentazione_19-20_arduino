// Variable resistance stretch cord testing code! Made Jan 29 by Erin Gee for Instructables

int sensorPin = A0;    // select the input pin for the potentiometer
int scaleValue = 4;  //This scales the input into something the LED can handle
float sensorValue, fadeValue;

void setup() {
  // initialize serial communications at 9600 bps:
  Serial.begin(9600);   
}

void loop() {
  
  // read the value from the sensor:
  sensorValue = analogRead(sensorPin);
  // divide it into a value from 0-255
  fadeValue = sensorValue/scaleValue;
  //write these values to Serial Window
  Serial.println(fadeValue);
  delay(5);                
}
