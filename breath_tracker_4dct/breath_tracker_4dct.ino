// Variable resistance stretch cord testing code! Made Jan 29 by Erin Gee for Instructables

int sensorPin = A0;    // select the input pin for the potentiometer
int scaleValue = 4;  //This scales the input into something the LED can handle
float sensorValue, fadeValue;
const int threshold = 144; 

const int buzzer = 9; //buzzer to arduino pin 9

void setup() {
  // initialize serial communications at 9600 bps:
  Serial.begin(9600);
  pinMode(buzzer, OUTPUT); // Set buzzer - pin 9 as an output
}

void loop() {
  
  // read the value from the sensor:
  sensorValue = analogRead(sensorPin);
  // divide it into a value from 0-255
  fadeValue = sensorValue/scaleValue;
  //write these values to Serial Window
  Serial.println(fadeValue);

  if (fadeValue > threshold){
    tone(buzzer, 1000);
    delay(10);
    noTone(buzzer);
  }
  else{
    noTone(buzzer);
  }
  
  delay(5);                
}
